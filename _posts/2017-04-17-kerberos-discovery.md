---
layout: post
title:  "Kerberos Service Discovery"
date:   2017-04-17 16:35:00 -0500
comments : false
categories: kerberos
---

An important part of ensuring that Kerberos clients work properly is that they
are able to locate the correct KDCs to communicate with. The common way is to
rely on DNS queries for SRV records. For an MIT Kerberos client this is enabled
with the krb5.conf option `dns_lookup_kdc`. An example of the Kerberos SRV
records:

	_kerberos._tcp.example.com SRV 0 0 88 192.168.1.10
	_kerberos._udp.example.com SRV 0 0 88 192.168.1.10
	_kpasswd._tcp.example.com SRV 0 0 464 192.168.1.10

There are a few drawbacks with SRV records. The first is that the client may
need to fall back to another server and must make additional DNS queries.
Second, the DNS administrator may want clients to prefer TCP over UDP, and being
that the TCP and UDP records are separate lookups there is no control over the
client’s preference. Third, the [MS-KKDCP][kkdcp] protocol is a way for clients to
communicate with a KDC through a HTTPS based proxy, and the SRV record type is
insufficient for informing clients of KKDCP servers.

The solution for this is a new [URI record][uri] type that is designed to inform the
client of all Kerberos servers for a domain through a single query. 

	_kerberos.EXAMPLE.COM URI 10 1 krb5srv:m:tcp:kdc.example.com
			      URI 20 1 krb5srv:m:udp:kdc.example.com:89
			      URI 30 1 krb5srv::kkdcp:https://krbproxy.example.com/KdcProxy

Making a DNS query for the URI record `_kerberos.EXAMPLE.COM`, the client
receives all three server entries. Each defines a priority (lowest goes first),
weight (highest goes first among identical priority), and a specially formatted
entry.  An entry begins with the `krb5srv` tag. The second field contains flag
characters that indicate server attributes.  Currently there is only one flag,
`m`, to inform the client that the entry is for a master server. If a MIT
kerberos client receives an incorrect password error from a non-master, it will
retry against a master. With SRV records, this requires an additional DNS query.
The third field defines the transport type; TCP, UDP, or MS-KKDCP. The last
field holds the name or address of the server and optionally a port number. If
the entry is for an MS-KKDCP server then this takes form as an HTTPS URL with
optional port and path components.

This method of Kerberos service discovery has been adopted by the IETF’s KITTEN
working group as a proposed standard. The current version of the internet draft
is [draft-ietf-kitten-krb-service-discovery-00][draft]

[MIT krb5][k5] 1.15 already contains support for URI service discovery by clients, and
is enabled (on by default) through the krb5.conf option `dns_uri_lookup`.

[draft]: https://tools.ietf.org/html/draft-ietf-kitten-krb-service-discovery-00
[kkdcp]: https://msdn.microsoft.com/en-us/library/hh554145.aspx
[k5]: https://web.mit.edu/kerberos/
[uri]: http://tools.ietf.org/html/rfc7553.html
